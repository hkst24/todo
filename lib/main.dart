import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class TodoItem {
  String title;
  String description;
  bool isDone;

  TodoItem({required this.title, required this.description, this.isDone = false});

  static final TodoItem empty = TodoItem(title: '', description: '');

  Map<String, dynamic> toJson() => {
    'title': title,
    'description': description,
    'isDone': isDone,
  };

  factory TodoItem.fromJson(Map<String, dynamic> json) => TodoItem(
    title: json['title'],
    description: json['description'],
    isDone: json['isDone'],
  );
}

void main() {
  runApp(const TodoApp());
}

class TodoApp extends StatelessWidget {
  const TodoApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const CupertinoApp(
      title: 'Todo',
      home: TodoList(),
    );
  }
}

class TodoList extends StatefulWidget {
  const TodoList({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _TodoListState createState() => _TodoListState();
}

class _TodoListState extends State<TodoList> {
  List<TodoItem> items = [];

  @override
  void initState() {
    super.initState();
    loadItems();
  }

  void saveItems() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('items', jsonEncode(items.map((item) => item.toJson()).toList()));
  }

  void loadItems() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? itemsString = prefs.getString('items');
    if (itemsString != null) {
      var itemsJson = jsonDecode(itemsString) as List;
      setState(() {
        items = itemsJson.map((itemJson) => TodoItem.fromJson(itemJson)).toList();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: const Text('Список задач'),
        trailing: CupertinoButton(
          padding: const EdgeInsets.all(0),
          child: const Icon(CupertinoIcons.add, color: CupertinoColors.activeBlue),
          onPressed: () async {
            TodoItem? newTodo = await showCupertinoDialog(
              context: context,
              builder: (context) => const AddTodoDialog(),
            );
            if (newTodo != null && newTodo.title.isNotEmpty && newTodo.description.isNotEmpty) {
              setState(() {
                items.add(newTodo);
              });
              saveItems();
            }
          },
        ),
      ),
      child: items.isEmpty
        ? const Center(child: Text('Нет запланированных задач'))
        : ListView(
            children: <Widget>[
              const Text('Текущие задачи', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              ...items.where((item) => !item.isDone).map(buildItem).toList(),
              const Text('Завершенные задачи', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              ...items.where((item) => item.isDone).map(buildItem).toList(),
            ],
          ),
    );
  }

  Widget buildItem(TodoItem item) {
    return Material(
      child: ListTile(
        title: Text(item.title),
        subtitle: Text(item.description),
        tileColor: CupertinoColors.systemGrey,
        onTap: item.isDone ? null : () async {
          TodoItem? updatedTodo = await showCupertinoDialog(
            context: context,
            builder: (context) => EditTodoDialog(initialTodo: item),
          );
          if (updatedTodo == TodoItem.empty) {
            setState(() {
              items.remove(item);
            });
          } else if (updatedTodo != null) {
            setState(() {
              item.title = updatedTodo.title;
              item.description = updatedTodo.description;
            });
          }
          saveItems();
        },
        trailing: IconButton(
          icon: Icon(item.isDone ? CupertinoIcons.xmark : CupertinoIcons.check_mark),
          onPressed: () {
            setState(() {
              if (item.isDone) {
                items.remove(item);
              } else {
                item.isDone = true;
              }
            });
            saveItems();
          },
        ),
      ),
    );
  }
}

class AddTodoDialog extends StatefulWidget {
  const AddTodoDialog({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _AddTodoDialogState createState() => _AddTodoDialogState();
}

class _AddTodoDialogState extends State<AddTodoDialog> {
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: const Text('Добавить задачу'),
      content: Column(
        children: <Widget>[
          CupertinoTextField(
            controller: titleController,
            autofocus: true,
            placeholder: 'Название задачи',
          ),
          const SizedBox(height: 20),
          CupertinoTextField(
            controller: descriptionController,
            placeholder: 'Описание задачи',
            maxLines: null,
          ),
        ],
      ),
      actions: <Widget>[
        CupertinoDialogAction(
          child: const Text('Отмена'),
          onPressed:
              () => Navigator.of(context).pop(),
        ),
        CupertinoDialogAction(
          child: const Text('Добавить'),
          onPressed:
              () => Navigator.of(context).pop(TodoItem(title:titleController.text,description:descriptionController.text)),
        ),
      ],
    );
  }
}


class EditTodoDialog extends StatefulWidget {
  final TodoItem initialTodo;

  const EditTodoDialog({super.key, required this.initialTodo});

  @override
  // ignore: library_private_types_in_public_api
  _EditTodoDialogState createState() => _EditTodoDialogState();
}

class _EditTodoDialogState extends State<EditTodoDialog> {
  TextEditingController? titleController;
  TextEditingController? descriptionController;

  @override
  void initState() {
    super.initState();
    titleController = TextEditingController(text: widget.initialTodo.title);
    descriptionController = TextEditingController(text: widget.initialTodo.description);
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: const Text('Редактировать задачу'),
      content: Column(
        children: <Widget>[
          CupertinoTextField(
            controller: titleController,
            autofocus: true,
            placeholder: 'Название задачи',
          ),
          CupertinoTextField(
            controller: descriptionController,
            placeholder: 'Описание задачи',
            maxLines: null,
          ),
        ],
      ),
      actions: <Widget>[
        CupertinoDialogAction(
          child: const Text('Отмена'),
          onPressed:
              () => Navigator.of(context).pop(),
        ),
        CupertinoDialogAction(
          isDestructiveAction:true,
          onPressed:
              () => Navigator.of(context).pop(TodoItem.empty),
          child: const Text('Удалить'),
        ),
        CupertinoDialogAction(
          child: const Text('Сохранить'),
          onPressed:
              () => Navigator.of(context).pop(TodoItem(title:titleController!.text,description:descriptionController!.text)),
        ),
      ],
    );
  }
}